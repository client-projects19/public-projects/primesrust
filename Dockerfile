FROM lukemathwalker/cargo-chef:latest-rust-bookworm AS chef
WORKDIR /app

FROM chef AS planner
COPY . .
RUN cargo chef prepare --recipe-path recipe.json

FROM chef AS builder 
COPY --from=planner /app/recipe.json recipe.json
# Build dependencies - this is the caching Docker layer!
RUN cargo chef cook --release --recipe-path recipe.json
# Build application
COPY . .
RUN cargo build --release --bin primes

# We do not need the Rust toolchain to run the binary!
FROM debian:bookworm AS runtime
RUN apt-get update
RUN apt-get -y install pkg-config libssl-dev ca-certificates
WORKDIR /app
COPY Rocket.toml /app/
COPY .env /app/
COPY static /app/static/
COPY templates /app/templates/
COPY migrations /app/migrations/
COPY --from=builder /app/target/release/primes /usr/local/bin
ENTRYPOINT ["/usr/local/bin/primes"]
