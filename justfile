default:
    @just --list

dev:
    cargo watch -c -x run

nix:
    nix-shell --command zsh --run "cargo watch -c -x run"

docker_build LABEL="temp":
    docker build . -t {{LABEL}}

docker_run LABEL="temp": docker_build
    docker run temp
