use rocket::http::Status;
use rocket_db_pools::sqlx::Error;

use crate::work_unit::WorkUnitBuilderError;

#[derive(Debug, PartialEq)]
pub enum CustomError {
    Status(Status),
    WorkUnitBuilderError,
    Other,
}

impl CustomError {
    pub fn this_error_needs_improving() -> Self {
        Self::Other
    }
}

impl From<WorkUnitBuilderError> for CustomError {
    fn from(value: WorkUnitBuilderError) -> Self {
        Self::WorkUnitBuilderError
    }
}

impl From<CustomError> for Status {
    fn from(value: CustomError) -> Self {
        match value {
            CustomError::Status(x) => x,
            CustomError::Other => Status::NotFound,
            CustomError::WorkUnitBuilderError => Status::InternalServerError,
        }
    }
}

impl From<Status> for CustomError {
    fn from(status: Status) -> Self {
        CustomError::Status(status)
    }
}

impl From<Error> for CustomError {
    fn from(value: Error) -> Self {
        match value {
            Error::RowNotFound => Status::NotFound,
            Error::TypeNotFound { type_name: _ } => Status::NotFound,
            Error::ColumnIndexOutOfBounds { index: _, len: _ } => Status::NotFound,
            Error::ColumnNotFound(_) => Status::NotFound,
            _ => Status::InternalServerError,
        }
        .into()
    }
}
