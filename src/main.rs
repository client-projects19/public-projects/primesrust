use std::ops::Range;

use rocket::{fs::FileServer, http::Status, serde::json::Json};

use rocket_cors::{AllowedOrigins, CorsOptions};
use rocket_db_pools::Database;
use rocket_dyn_templates::{context, Template};
use rocket_sqlx_migrator::SqlxMigrator;

mod db;
use db::PrimeDB;

mod auth;
use auth::User;

pub mod prime;
use prime::{get_primes, return_primes};

mod custom_error;

mod work_unit;
use serde::{Deserialize, Serialize};
pub use work_unit::WorkUnit;

// Util
mod unwrap_bound;

#[macro_use]
extern crate rocket;

#[derive(Debug, Serialize, Deserialize)]
struct Message<'a> {
    pub message: &'a str,
    pub colour: &'a str,
}

#[get("/msg")]
fn msg<'a>() -> Json<Message<'a>> {
    Json(Message {
        message: "This is using the new auth system!",
        colour: "orange",
    })
}

#[get("/work_unit")]
async fn get_work_unit(db: &PrimeDB) -> Result<Json<WorkUnit>, Status> {
    Ok(Json(WorkUnit::get_available(db).await?))
}

// Could in future also return next work unit
#[post("/work_unit/<work_id>", data = "<primes>")]
async fn post_work_unit_logged_in(
    db: &PrimeDB,
    work_id: i64,
    user: Option<User>,
    primes: Json<Vec<i64>>,
) -> Result<(), Status> {
    WorkUnit::from_id(&db, work_id)
        .await?
        .upload_primes(&db, user, primes.0)
        .await?;

    Ok(())
}

#[post("/prime", data = "<primes>", rank = 2)]
async fn post_work_unit(db: &PrimeDB, primes: Json<Vec<i64>>) -> Result<(), Status> {
    Ok(return_primes(db, primes.into_inner()).await?)
}

#[get("/prime/<min>/<max>")]
async fn get_prime_search(db: &PrimeDB, min: i64, max: i64) -> Result<Json<Vec<i64>>, Status> {
    Ok(Json(get_primes(db, min..max).await?))
}

#[get("/prime?<form..>")]
async fn get_prime_form(db: &PrimeDB, form: Range<i64>) -> Result<Json<Vec<i64>>, Status> {
    Ok(Json(get_primes(db, form).await?))
}

#[get("/prime.html?<form..>")]
async fn get_prime_form_html(db: &PrimeDB, form: Range<i64>) -> Result<Template, Status> {
    let primes = get_primes(db, form).await?;
    Ok(Template::render(
        "return_primes",
        context! {
            primes
        },
    ))
}

#[get("/")]
fn index(user: User) -> Template {
    Template::render(
        "index",
        context! {
            user,
        },
    )
}

#[get("/", rank = 2)]
fn index_auth() -> Template {
    Template::render("index", context! {})
}

#[get("/calculate")]
fn calculate(user: Option<User>) -> Template {
    Template::render("calculate", context! {user})
}

#[get("/downloads")]
fn downloads(user: Option<User>) -> Template {
    Template::render("downloads", context! {user})
}

#[get("/empty")]
fn empty() -> Template {
    Template::render("empty", context! {})
}

#[launch]
fn rocket() -> _ {
    let allowed_origins =
        AllowedOrigins::some_regex(&[r"^https://.*\.greensiren\.co\.uk$", r"localhost"]);

    rocket::build()
        .attach(PrimeDB::init())
        .attach(SqlxMigrator::<PrimeDB>::migrate())
        .attach(Template::fairing())
        .attach(auth::fairing())
        .attach(
            CorsOptions {
                allowed_origins,
                ..Default::default()
            }
            .to_cors()
            .unwrap(),
        )
        .mount("/static/", FileServer::from("./static/"))
        .mount(
            "/",
            routes![
                index,
                index_auth,
                get_work_unit,
                post_work_unit,
                post_work_unit_logged_in,
                get_prime_search,
                msg,
                calculate,
                downloads,
                empty,
                get_prime_form,
                get_prime_form_html,
            ],
        )
}

#[cfg(test)]
mod test {
    use crate::db::DB;
    use crate::rocket;
    use rocket::{http::Status, local::blocking::Client};
    use sqlx::Pool;

    use crate::{
        custom_error::CustomError,
        prime::{get_primes, return_primes},
        work_unit::WorkUnit,
    };

    #[test]
    #[ignore = "This does not test anything"]
    fn post_prime_numbers() {
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        {
            let prime_list = vec![3, 5, 7];
            let prime_list = serde_json::to_string(&prime_list).unwrap();
            println!("{prime_list}");

            let response = client.post(uri!("/prime")).body(prime_list).dispatch();

            assert_eq!(response.status(), Status::Ok);
        }
        let _rocket = client.terminate();
    }

    #[sqlx::test]
    async fn integration_test(conn: Pool<DB>) -> Result<(), CustomError> {
        let work_unit = WorkUnit::get_available(&conn).await?;
        println!("{work_unit:?}");
        assert_eq!(work_unit.get_id(), 2);

        return_primes(&conn, vec![200001]).await?;
        let primes = get_primes(&conn, 0..100000).await?;
        println!("{primes:?}");

        let work_unit = WorkUnit::get_available(&conn).await?;
        println!("{work_unit:?}");
        assert_eq!(work_unit.get_id(), 3);

        return_primes(&conn, vec![400001]).await?;
        let primes = get_primes(&conn, 0..1000000).await?;
        println!("{primes:?}");

        let work_unit = WorkUnit::get_available(&conn).await?;
        println!("{work_unit:?}");
        assert_eq!(work_unit.get_id(), 4);

        return_primes(&conn, vec![600001]).await?;
        let primes = get_primes(&conn, 0..100000).await?;
        println!("{primes:?}");

        let work_unit = WorkUnit::get_available(&conn).await?;
        println!("{work_unit:?}");
        assert_eq!(work_unit.get_id(), 5);

        Ok(())
    }
}
