use rocket_db_pools::Database;
use sqlx::{Pool, Postgres, Sqlite};

pub type DB = Postgres;

#[derive(Database)]
#[database("prime")]
pub struct PrimeDB(Pool<DB>);
