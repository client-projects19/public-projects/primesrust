use std::ops::{Range, RangeBounds};

use sqlx::{Pool, QueryBuilder};

use crate::{custom_error::CustomError, db::DB, unwrap_bound::unwrap_bound};

pub async fn return_primes(conn: &Pool<DB>, primes: Vec<i64>) -> Result<(), CustomError> {
    let mut query_builder = QueryBuilder::new("INSERT INTO PrimeNumber (prime_number) ");

    query_builder.push_values(primes, |mut b, prime| {
        b.push_bind(prime);
    });

    let query = query_builder.build();

    query.execute(conn).await?;

    Ok(())
}

pub async fn get_primes(conn: &Pool<DB>, range: Range<i64>) -> Result<Vec<i64>, CustomError> {
    let min = unwrap_bound(range.start_bound());
    let max = unwrap_bound(range.end_bound());

    Ok(sqlx::query!(
        "SELECT prime_number FROM PrimeNumber WHERE $1 <= prime_number AND prime_number < $2",
        min,
        max,
    )
    .fetch_all(conn)
    .await
    .into_iter()
    .flatten()
    .map(|r| r.prime_number)
    .collect())
}

#[sqlx::test]
async fn test_return_primes(conn: Pool<DB>) -> Result<(), CustomError> {
    let primes = vec![3, 5, 7, 11];

    return_primes(&conn, primes).await
}

#[sqlx::test]
async fn test_get_primes(conn: Pool<DB>) -> Result<(), CustomError> {
    let primes = vec![3, 5, 7, 11];

    return_primes(&conn, primes).await?;

    let primes = get_primes(&conn, 3..7).await?;

    println!("{primes:?}");

    assert_eq!(primes, vec![3, 5]);
    Ok(())
}
