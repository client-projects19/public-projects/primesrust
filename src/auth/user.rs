use std::fmt::Display;

use reqwest::header::{AUTHORIZATION, USER_AGENT};
use rocket::{
    http::{Cookie, SameSite, Status},
    request::{self, FromRequest},
    Request,
};
use serde::{Deserialize, Serialize};
use sha256::digest;
use sqlx::{FromRow, Row};

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct User {
    #[serde(skip)]
    token: String,
    #[serde(skip_deserializing)]
    icon_url: String,
    sub: String,
    email: String,
    preferred_username: String,
}

impl Display for User {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.preferred_username)
    }
}

pub fn option(user: &Option<User>) -> String {
    if let Some(user) = user {
        user.get_username()
    } else {
        "Anonymous".to_string()
    }
}

// {"sub", "email", "ruben.john.ward@gmail.com", "email_verified", "name", "given_name", "preferred_username", "nickname", "groups"}

impl User {
    fn new(
        token: String,
        icon_url: String,
        sub: String,
        preferred_username: String,
        email: String,
    ) -> Self {
        Self {
            token,
            icon_url,
            preferred_username,
            email,
            sub,
        }
    }

    pub fn sub(&self) -> String {
        self.sub.clone()
    }

    pub fn get_username(&self) -> String {
        self.preferred_username.clone()
    }

    async fn from_server(token: String) -> anyhow::Result<Self> {
        let client = reqwest::Client::new();
        let user: User = client
            .get("https://authentik.morphtollon.co.uk/application/o/userinfo/")
            .header(AUTHORIZATION, format!("Bearer {}", token))
            .header(USER_AGENT, "primes")
            .send()
            .await?
            // Response handling
            .json()
            .await?;

        let email_hash = digest(&user.email);
        let icon_url = format!("https://gravatar.com/avatar/{email_hash}");

        Ok(Self {
            token,
            email: user.email,
            icon_url,
            preferred_username: user.preferred_username,
            sub: user.sub,
        })
    }
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for User {
    type Error = ();

    async fn from_request(request: &'r Request<'_>) -> request::Outcome<User, ()> {
        let cookies = request.cookies();

        let token = cookies.get("access_token").map(|x| x.value().to_string());
        let name = cookies
            .get("preferred_username")
            .map(|x| x.value().to_string());
        let icon_url = cookies.get("icon_url").map(|x| x.value().to_string());
        let email = cookies.get("email").map(|x| x.value().to_string());
        let sub = cookies.get("sub").map(|x| x.value().to_string());

        // println!("{token:?}\n{name:?}\n{icon_url:?}");

        if let (Some(token), Some(name), Some(icon_url), Some(email), Some(sub)) =
            (token.clone(), name, icon_url, email, sub)
        {
            // Clone is here cause i can't be bothered to use &str in the User type, deal with it...
            return request::Outcome::Success(Self::new(token, icon_url, sub, name, email));
        } else if let Some(token) = token {
            println!("Requesting UserInfo from Server");
            if let Ok(user) = Self::from_server(token).await {
                cookies.add(
                    Cookie::build(("preferred_username", user.preferred_username.clone()))
                        .same_site(SameSite::Lax),
                );
                cookies.add(
                    Cookie::build(("icon_url", user.icon_url.clone())).same_site(SameSite::Lax),
                );
                cookies.add(Cookie::build(("email", user.email.clone())).same_site(SameSite::Lax));
                cookies.add(Cookie::build(("sub", user.sub.clone())).same_site(SameSite::Lax));
                return request::Outcome::Success(user);
            }
        }

        request::Outcome::Forward(Status::Unauthorized)
    }
}

impl<'r, R> FromRow<'r, R> for User
where
    R: Row,
    String: sqlx::Decode<'r, <R as Row>::Database> + sqlx::Type<<R as Row>::Database>,
    for<'a> &'a str: sqlx::ColumnIndex<R>,
{
    fn from_row(row: &'r R) -> Result<Self, sqlx::Error> {
        let token: String = row.try_get("token")?;

        let rt = rocket::tokio::runtime::Builder::new_current_thread()
            .build()
            .unwrap();

        println!("Requesting UserInfo From server, loading from DB");
        Ok(rt.block_on(Self::from_server(token)).unwrap())
    }
}

// impl FromRow<'_, SqliteRow> for Foo {
//     fn from_row(row: &SqliteRow) -> sqlx::Result<Self> {
//         Ok(Self {
//             bar: MyCustomType {
//                 custom: row.try_get("custom")?
//             }
//         })
//     }
// }
