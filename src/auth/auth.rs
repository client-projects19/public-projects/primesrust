use rocket::{
    fairing::{AdHoc, Fairing},
    http::{Cookie, CookieJar, SameSite},
    response::Redirect,
};
use rocket_oauth2::{OAuth2, TokenResponse};

struct Authentik;

#[get("/login")]
fn login() -> Redirect {
    Redirect::to("/login/authentik")
}

#[get("/login/authentik")]
fn login_authentik(oauth2: OAuth2<Authentik>, cookies: &CookieJar<'_>) -> Redirect {
    oauth2
        .get_redirect(cookies, &["email", "openid", "profile"])
        .unwrap()
}

#[get("/auth/authentik")]
fn callback_authentik(token: TokenResponse<Authentik>, cookies: &CookieJar<'_>) -> Redirect {
    cookies.add(
        Cookie::build(("access_token", token.access_token().to_string()))
            .same_site(SameSite::Lax)
            .build(),
    );
    Redirect::to("/")
}

#[get("/logout")]
fn logout(cookies: &CookieJar<'_>) -> Redirect {
    cookies.remove(Cookie::from("access_token"));
    cookies.remove(Cookie::from("preferred_username"));
    cookies.remove(Cookie::from("icon_url"));
    cookies.remove(Cookie::from("email"));
    Redirect::to("/")
}

#[get("/logout/full")]
fn logout_full(cookies: &CookieJar<'_>) -> Redirect {
    cookies.remove(Cookie::from("access_token"));
    cookies.remove(Cookie::from("preferred_username"));
    cookies.remove(Cookie::from("icon_url"));
    cookies.remove(Cookie::from("email"));
    Redirect::to("https://authentik.morphtollon.co.uk/application/o/primes/end-session/")
}

pub fn fairing() -> impl Fairing {
    AdHoc::on_ignite("Authentik OAuth2", |rocket| async {
        rocket
            .mount(
                "/",
                rocket::routes![
                    login,
                    login_authentik,
                    callback_authentik,
                    logout,
                    logout_full
                ],
            )
            .attach(OAuth2::<Authentik>::fairing("authentik"))
    })
}
