use chrono::{DateTime, Utc};
use derive_builder::Builder;
use serde::{Deserialize, Serialize};
use sqlx::{FromRow, Pool, QueryBuilder, Row};
use std::ops::{Range, RangeBounds};

use crate::{
    auth::{option, User},
    custom_error::CustomError,
    db::DB,
    prime::return_primes,
    unwrap_bound::unwrap_bound,
};

const WORK_UNIT_OFFSET: i64 = 500_000;

#[derive(Debug, Builder, Serialize)]
#[builder(pattern = "owned", derive(Deserialize))]
pub struct WorkUnit {
    id: i64,
    range: Range<i64>,
    #[serde(skip)]
    #[builder_field_attr(serde(skip))]
    conn: Pool<DB>,
}

// FYI: all the comments here are leftovers of the planning, but i've left them in 'cause
// they might be helpful

/// The new API
impl WorkUnit {
    // Constructor
    // update work unit
    // - update reporter
    // - update verifier
    // - get/set primes

    async fn set_primes(&self, primes: Vec<i64>) -> Result<(), CustomError> {
        let mut query_builder = QueryBuilder::new("DELETE FROM PrimeNumber WHERE $1 <= prime_number AND prime_number < $2; INSERT INTO PrimeNumber (prime_number) ");

        query_builder.push_bind(self.range.start);
        query_builder.push_bind(self.range.end);
        query_builder.push_values(primes, |mut b, prime| {
            b.push_bind(prime);
        });

        let query = query_builder.build();

        query.execute(&self.conn).await?;

        Ok(())
    }

    async fn get_primes(&self) -> Result<Vec<i64>, CustomError> {
        Ok(sqlx::query!(
            "SELECT prime_number FROM PrimeNumber WHERE $1 <= prime_number AND prime_number < $2",
            self.range.start,
            self.range.end,
        )
        .fetch_all(&self.conn)
        .await
        .into_iter()
        .flatten()
        .map(|r| r.prime_number)
        .collect())
    }

    async fn set_reporter(&self, reporter: Option<User>) -> Result<(), CustomError> {
        sqlx::query!(
            "UPDATE WorkUnit SET reporter = $1, date_reported = $2 WHERE id = $3",
            reporter.map(|r| r.sub()),
            chrono::offset::Utc::now(),
            self.id,
        )
        .execute(&self.conn)
        .await?;

        Ok(())
    }

    async fn set_verifier(&self, verifier: Option<User>) -> Result<(), CustomError> {
        sqlx::query!(
            "UPDATE WorkUnit SET verifier = $1, date_reported = $2 WHERE id = $3",
            verifier.map(|v| v.sub()),
            chrono::offset::Utc::now(),
            self.id,
        )
        .execute(&self.conn)
        .await?;

        Ok(())
    }

    async fn get_status(&self) -> Result<Status, CustomError> {
        // Check in reverse order: verified, reported, primes exist
        // Check if verifier set
        // Check if reporter set
        // - OR check if primes exist in range

        let data = sqlx::query!(
            "SELECT reporter, verifier FROM WorkUnit WHERE id = $1",
            self.id,
        )
        .fetch_one(&self.conn)
        .await?;

        if data.verifier.is_some() {
            return Ok(Status::Verified);
        }

        if data.reporter.is_some() {
            return Ok(Status::Reported);
        }

        let count = sqlx::query!(
            "SELECT COUNT(*) FROM PrimeNumber WHERE $1 <= prime_number AND prime_number < $2",
            self.range.start,
            self.range.end,
        )
        .fetch_one(&self.conn)
        .await?
        .count
        .ok_or(CustomError::this_error_needs_improving())?;

        if count != 0 {
            return Ok(Status::Reported);
        }

        Ok(Status::Unreported)
    }
}

/// Getter Functions
impl WorkUnit {
    pub fn get_id(&self) -> i64 {
        self.id
    }

    pub fn get_range(&self) -> &Range<i64> {
        &self.range
    }
}

enum Status {
    Unreported,
    Reported,
    Verified,
}

/// Get Work Unit
impl WorkUnit {
    async fn issue_new(conn: &Pool<DB>) -> Result<WorkUnit, CustomError> {
        let work_unit = Self::get_most_recent(conn).await?;
        let start = unwrap_bound(work_unit.range.end_bound());

        let work_unit: WorkUnit = sqlx::query_as::<DB, WorkUnitBuilder>(
            "INSERT INTO WorkUnit(min_range, max_range, date_sent) VALUES ($1, $2, $3) RETURNING *",
        )
        .bind(start)
        .bind(start + WORK_UNIT_OFFSET)
        .bind(chrono::offset::Utc::now())
        .fetch_one(conn)
        .await?
        // Builder for Work Unit
        .conn(conn.clone())
        .build()?;

        println!("New Issue: {:?}", work_unit.id);

        Ok(work_unit)
    }

    async fn get_rankings(&self) -> Result<Vec<User>, CustomError> {
        let users: Vec<User> = sqlx::query_as(
            "
                SELECT reporter, COUNT(*) AS counted
                FROM   WorkUnit
                WHERE  reporter IS NOT NULL  -- exclude NULL
                GROUP  BY reporter
                ORDER  BY counted DESC, reporter  -- break ties in deterministic fashion
                LIMIT  10;
            ",
        )
        .fetch_all(&self.conn)
        .await?;

        Ok(users)
    }

    pub async fn from_id(conn: &Pool<DB>, id: i64) -> Result<WorkUnit, CustomError> {
        let work_unit: WorkUnit =
            sqlx::query_as::<DB, WorkUnitBuilder>("SELECT * FROM WorkUnit WHERE id = $1 LIMIT 1")
                .bind(id)
                .fetch_one(conn)
                .await?
                // Builder for Work Unit
                .conn(conn.clone())
                .build()?;

        Ok(work_unit)
    }

    pub async fn upload_primes(
        &self,
        conn: &Pool<DB>,
        user: Option<User>,
        mut primes: Vec<i64>,
    ) -> Result<(), CustomError> {
        primes.sort_unstable();

        // Check if primes in range of work unit - this could be re-written to be better, the problem is: My energy = ↓
        if !(primes.first() >= Some(&self.range.start)
            || dbg!(primes.last()) < dbg!(Some(&self.range.end)))
        {
            println!("Out of bounds, rejected");
            return Err(CustomError::this_error_needs_improving());
        }

        // Check if work unit has been unreported, reported or verified yet
        use Status as S;
        match self.get_status().await? {
            // Upload primes, set reporter
            S::Unreported => {
                println!(
                    "WorkUnit {}: Unreported -> Reported, Reporter: {}",
                    self.id,
                    option(&user)
                );
                return_primes(conn, primes).await?;
                self.set_reporter(user).await?;
            }
            S::Reported => {
                let reported_primes = self.get_primes().await?;
                if reported_primes == primes {
                    println!(
                        "WorkUnit {}: Reported -> Verified, Verifier: {}",
                        self.id,
                        option(&user)
                    );
                    self.set_verifier(user).await?;
                } else {
                    println!(
                        "WorkUnit {}: Reported -> Reported, Reported: {} (Collision ocurred)",
                        self.id,
                        option(&user)
                    );
                    self.set_reporter(user).await?;
                    return_primes(conn, primes).await?;
                }
            }
            S::Verified => {
                panic!("There should be no work units being sent out with verified status")
            }
        }
        // Reported
        //  Download primes
        //  Compare against local ones
        //  If match, upload work unit verifier
        // Verified
        //  This should not happen, trigger a warning
        //  discard for now
        Ok(())
    }

    pub async fn get_most_recent(conn: &Pool<DB>) -> Result<WorkUnit, CustomError> {
        let work_unit: WorkUnit = sqlx::query_as::<DB, WorkUnitBuilder>(
            "SELECT * FROM WorkUnit ORDER BY id DESC LIMIT 1",
        )
        .fetch_one(conn)
        .await?
        // Builder for Work Unit
        .conn(conn.clone())
        .build()?;

        Ok(work_unit)
    }

    async fn issue_unfinished(conn: &Pool<DB>) -> Result<WorkUnit, CustomError> {
        let work_unit = sqlx::query_as::<DB, WorkUnitBuilder>(
            "
                UPDATE WorkUnit
                SET date_sent = NOW()
                WHERE id = (
                    SELECT id FROM WorkUnit
                    WHERE WorkUnit.min_range > (
                        SELECT prime_number FROM PrimeNumber
                        ORDER BY prime_number DESC
                        LIMIT 1
                    )
                    AND date_sent::TIMESTAMPTZ < (NOW()::TIMESTAMPTZ - interval '5 minutes')
                    ORDER BY date_sent ASC
                    LIMIT 1
                )
                RETURNING *;
            ",
        )
        .fetch_one(conn)
        .await?
        // Builder for Work Unit
        .conn(conn.clone())
        .build()?;

        println!("Old Issue: {:?}", work_unit.id);

        Ok(work_unit)
    }

    pub async fn get_available(conn: &Pool<DB>) -> Result<WorkUnit, CustomError> {
        Ok(match Self::issue_unfinished(conn).await {
            Ok(work_unit) => work_unit,
            _ => Self::issue_new(conn).await?,
        })
    }
}

impl<'r, R> FromRow<'r, R> for WorkUnitBuilder
where
    R: Row,
    &'r str: sqlx::ColumnIndex<R>,
    i64: sqlx::decode::Decode<'r, R::Database>,
    i64: sqlx::types::Type<R::Database>,
    DateTime<Utc>: sqlx::decode::Decode<'r, R::Database>,
    DateTime<Utc>: sqlx::types::Type<R::Database>,
{
    fn from_row(row: &'r R) -> Result<Self, sqlx::Error> {
        Ok(WorkUnitBuilder::default()
            .id(row.try_get("id")?)
            .range(row.try_get("min_range")?..row.try_get("max_range")?))
    }
}

#[cfg(test)]
mod test {
    use sqlx::Pool;

    use crate::custom_error::CustomError;
    use crate::db::DB;
    use crate::work_unit::WorkUnit;

    #[sqlx::test]
    async fn get_prime(conn: Pool<DB>) -> Result<(), CustomError> {
        let prime_number = sqlx::query!("SELECT * FROM PrimeNumber")
            .fetch_one(&conn)
            .await?;

        println!("{prime_number:?}");

        Ok(())
    }

    #[sqlx::test]
    async fn get_work_unit(conn: Pool<DB>) -> Result<(), CustomError> {
        let work_unit: WorkUnit = sqlx::query_as("SELECT * FROM WorkUnit")
            .fetch_one(&conn)
            .await?;

        println!("{work_unit:?}");

        Ok(())
    }

    #[sqlx::test]
    async fn new_work_unit(conn: Pool<DB>) -> Result<(), CustomError> {
        let work_unit = WorkUnit::issue_new(&conn).await?;

        println!("{work_unit:?}");

        Ok(())
    }

    #[sqlx::test]
    async fn get_unfinished(conn: Pool<DB>) -> Result<(), CustomError> {
        let work_unit = WorkUnit::issue_unfinished(&conn).await?;

        println!("{work_unit:?}");

        assert_eq!(work_unit.get_id(), 2);
        Ok(())
    }
}
