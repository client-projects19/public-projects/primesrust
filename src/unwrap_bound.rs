use std::ops::Bound;

pub fn unwrap_bound<T>(bound: Bound<T>) -> T {
    match bound {
        Bound::Included(x) => x,
        Bound::Excluded(x) => x, // This is the one that we're using
        Bound::Unbounded => panic!("How the hell is there a range with no bound"),
    }
}
