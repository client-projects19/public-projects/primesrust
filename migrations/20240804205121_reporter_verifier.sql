-- Add migration script here
ALTER TABLE WorkUnit
ADD COLUMN reporter TEXT;

ALTER TABLE WorkUnit
ADD COLUMN date_reported TIMESTAMPTZ;

ALTER TABLE WorkUnit
ADD COLUMN verifier TEXT;

ALTER TABLE WorkUnit
ADD COLUMN date_verified TIMESTAMPTZ;
