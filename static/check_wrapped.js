function checkWrap() {
    const container = document.querySelector('header');
    const items = container.children;
    const containerHeight = container.clientHeight; // Get the height of the container
    let isWrapped = false;

    // Check if any item's offsetTop is greater than half the height of the container
    for (let i = 0; i < items.length; i++) {
        if (items[i].offsetTop > containerHeight / 2) {
            isWrapped = true;
            break;
        }
    }

    // Add or remove the 'wrapped' class based on the wrapping state
    if (isWrapped) {
        container.classList.add('wrapped');
    } else {
        container.classList.remove('wrapped');
    }
}

// Initial check
checkWrap();

// Check on window resize
window.addEventListener('resize', checkWrap);
