function updateClock(){
    time = new Date();
    document.getElementById('time').innerHTML = time.toLocaleString()
}

function updateMSG(msg){
    fetch('../msg')
	.then(data => data.json())
	.then((json) => {
        console.log(json)
        msg.innerHTML = json['message']
        msg.style.textDecorationColor = json['colour']
    })
    .catch((reason) => {
        console.log(reason, "\nWill just use default")
    })
}

window.onload = function main() {
    msg = document.getElementById('msg')
    // updateClock()
    // window.setInterval(updateClock, 1000)
    updateMSG(msg)
    window.setInterval(updateMSG, 600000)
}
