var work_unit_list = document.getElementsByClassName('work_units')[0]
var workUnit = document.getElementsByClassName('workUnit')[0]

const worker = new Worker('./static/minePrime.js')

var start = Math.ceil(work_unit_list.offsetHeight / work_unit_list.firstChild.offsetHeight)

var rec = document.getElementsByClassName('status rec')[0]
var calc = document.getElementsByClassName('status calc')[0]
var sub = document.getElementsByClassName('status sub')[0]
var work_units_calculated_html = document.getElementsByClassName('work_units_calculated')[0]
var primes_calculated_html = document.getElementsByClassName('primes_calculated')[0]
var primes_html = document.getElementsByClassName('primes')[0]
var stages = ["missing", "working", "done"]

function set_element_status(element, status) {
    var current_class = element.classList[2]
    element.classList.remove(current_class)

    element.classList.add(status)
}

let current_work_id = 0
let primes_calculated = 0
let total_work_units_calculated = 0
let total_primes_calculated = 0

worker.addEventListener("message", event => {
    if (event.data && event.data[1] == 'prime') {
        primes_html.innerHTML = event.data[0]
        primes_calculated_html.innerHTML = event.data[0] + total_primes_calculated
    }
    if (event.data && event.data[1] == 'currentWorkUnit') {
        current_work_id = event.data[0]
        workUnit.innerHTML = event.data[0]
    }

    if (event.data && event.data[1] == "Receiving") {
        set_element_status(rec, event.data[0])
    }
    if (event.data && event.data[1] == "Calculating") {
        set_element_status(calc, event.data[0])

        if (event.data[0] == "done" || event.data[0] == "missing") {
            primes_calculated = event.data[2]
            primes_html.innerHTML = primes_calculated
            total_primes_calculated += event.data[2]
            primes_calculated_html.innerHTML = total_primes_calculated
        }
    }
    if (event.data && event.data[1] == "Submitting") {
        set_element_status(sub, event.data[0])
        if (event.data[0] == "done") {
            let new_element = work_unit_list.insertBefore(
                document.createElement('div'), work_unit_list.firstChild
            );
            new_element.classList.add("background_card", "flex")
            new_element.innerHTML = `
                <p>Work Unit: ${current_work_id}</p>
                <p>Primes: ${primes_calculated}</p>
                <p>Submitted</p>
            `
    
            while (work_unit_list.children.length > Math.floor(work_unit_list.offsetHeight / work_unit_list.firstChild.offsetHeight)) {
                work_unit_list.removeChild(work_unit_list.lastChild)
            }
            start++
            total_work_units_calculated++
            work_units_calculated_html.innerHTML = total_work_units_calculated
        }
    }
});
