const delay = ms => new Promise(res => setTimeout(res, ms));

function calcPrimes(start, end) {
	var primeNumbers = []; /* Where the prime numbers are stored */
	for (var dividend = start; dividend <= end; dividend++) {
		var root = Math.sqrt(dividend);
		var isPrime = true;
		for (var divisor = 2; divisor <= root; divisor++) {
			if (dividend % divisor == 0) {
				isPrime = false;
				break;
			}
		}
		if (isPrime) {
			primeNumbers.push(dividend);
			if (primeNumbers.length % 100 == 0 && primeNumbers.length != 0) {
				postMessage([primeNumbers.length, 'prime'])
			}
		}
	}
	return primeNumbers
}

function send(data, channel) {
	postMessage([data, channel])
}

function launch_primes(json) {
	send("working", "Calculating")

	var start = json['range']['start']
	var end = json['range']['end']
	var workID = json['id']

	console.log(workID)

	// send(workID, 'currentWorkUnit')
	// send(workID + " (" + (end - start) + " primes)", 'prime')
	var primes = calcPrimes(start, end)
	var result = [primes, workID]

	postMessage(["done", "Calculating", primes.length])
	return result
}

function end_chain(data) {
	console.log("Chain Ended")
	try {
		console.log(data)
	} catch {}
	return { then: function() {} }
}

function result_to_server(result) {
	send("working", "Submitting")
	
	var result = fetch('../work_unit/' + result[1], {
		body: JSON.stringify(result[0]),
		method: 'POST'
	})
	.then(() => send("done", "Submitting"))

	return result
}

function reset_status() {
	postMessage(["missing", "Calculating", 0])
	send("missing", "Submitting")
}

async function runPrime() {

	send("working", "Receiving")

	await fetch('../work_unit')
		.then(data => data.json())
		.then(json => {
			send(json["id"], "currentWorkUnit")
			send("done", "Receiving")
			return json
		})
		.then(launch_primes, end_chain)
		.then(result_to_server, end_chain)
		.then(async () => await delay(500))
	
	reset_status()
}

async function main() {
	while (true) {
		await runPrime()
	}
}

main()